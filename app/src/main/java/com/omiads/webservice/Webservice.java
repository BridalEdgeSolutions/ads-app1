package com.omiads.webservice;

import android.graphics.Bitmap;
import android.os.Environment;
import android.util.Log;

import com.omiads.model.DevicePackages;
import com.omiads.model.DownloadDetails;
import com.omiads.model.FileData;
import com.omiads.model.Panel;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import static com.omiads.webservice.WebserviceConstant.CMS_URL;

/**
 * Created by kanak on 4/2/17.
 */

public class Webservice {
    public static Webservice webservice;
    public static Webservice getInstance() {
        if(webservice==null) {
            webservice = new Webservice();
        }
        return webservice;
    }

    public Panel getPanels(String imei_no) {
        int         responseCode        = 0;
        Panel       panel               = null;
        String      strResponse         = null;
        HttpURLConnection connection    = null;
        InputStream is                  = null;
        JSONObject dataObject           = null;
        try {
            String mainUrl = CMS_URL + "/panels?imei_no="+imei_no;
            JSONObject obj = new JSONObject();
            obj.put("imei_no", imei_no);
            Iterator<String> keys = obj.keys();
            String postStr = "";
            while (keys.hasNext()) {
                if(postStr.length() > 0)
                    postStr += "&";
                String key = keys.next();
                postStr = postStr + key + "=" + obj.getString(key);
            }
            //Log.d("OMIADS", " ============ " + mainUrl);
            URL url = new URL(mainUrl);
            connection = (HttpURLConnection) url.openConnection();
            responseCode = connection.getResponseCode();
            if(responseCode == HttpURLConnection.HTTP_OK) {
                is = connection.getInputStream();
                if(is != null) {
                    StringBuffer csBuffer = new StringBuffer();
                    InputStreamReader csReader = new InputStreamReader(is);
                    BufferedReader csBufferReader = new BufferedReader(csReader);
                    String out = null;
                    while ((out = csBufferReader.readLine()) != null) {
                        csBuffer.append(out);
                    }
                    strResponse = csBuffer.toString();
                    Log.d("OMIADS", " ====== Panels " + strResponse);
                    dataObject   = new JSONObject(strResponse);
                    if(dataObject != null && dataObject.has("device_id")) {
                        panel = new Panel("1", dataObject.getString("device_id"));
                    } else {
                        panel = new Panel("0", "");
                    }
                }
            } else {
                panel = new Panel("-1", "");
            }

            if(is != null) {
                is.close();
            }
        } catch (Exception e) {
            e.printStackTrace();
            panel = new Panel("-1", "");
        }
        return panel;
    }
    public DevicePackages getDevicePackages(String device_id) {
        DevicePackages          devicePackages  = null;
        int                     responseCode    = 0;
        Panel                   panel           = null;
        String                  strResponse     = null;
        HttpURLConnection       connection      = null;
        InputStream             is              = null;
        OutputStream            os              = null;

        try {
            File srcDirectory   = new File(Environment.getExternalStorageDirectory() + "/TricurveAdminPanel");
            if(!srcDirectory.exists()) {
                srcDirectory.mkdir();
            }
            //Log.d("OMIADS", " ======== DeviceId :  " + device_id);
            JSONObject obj = new JSONObject();
            obj.put(WebserviceConstant.CMS_DEVICE_ID_KEY, device_id);
            Iterator<String> keys = obj.keys();
            String postStr = "";
            while (keys.hasNext()) {
                if(postStr.length() > 0)
                    postStr += "&";
                String key = keys.next();
                postStr = postStr + key + "=" + obj.getString(key);
            }
            String mainUrl = CMS_URL + "/devicePackages";
            Log.d("OMIADS", " ======================== ");
            URL url = new URL(mainUrl);
            connection = (HttpURLConnection) url.openConnection();
            connection.setDoInput(true);
            connection.setConnectTimeout(15000);
            connection.setRequestProperty (WebserviceConstant.CMS_DEVICE_ID_KEY, device_id);
            connection.setRequestMethod("GET");
            connection.connect();
            responseCode = connection.getResponseCode();
            if(responseCode == HttpURLConnection.HTTP_OK) {
                is = connection.getInputStream();
                if(is != null) {
                    StringBuffer csBuffer = new StringBuffer();
                    InputStreamReader csReader = new InputStreamReader(is);
                    BufferedReader csBufferReader = new BufferedReader(csReader);
                    String out = null;
                    while ((out = csBufferReader.readLine()) != null) {
                        csBuffer.append(out);
                    }
                    strResponse = csBuffer.toString();
                    Log.d("OMIADS", " ====== devicePackages : " + strResponse);
                    JSONObject csDataObject = new JSONObject(strResponse);
                    //Log.d("OMIADS", " ====== packageId : " + csDataObject.getString("package_id"));
                    JSONObject csDataArray   = csDataObject.getJSONObject("day_data");
                    Iterator<String> iterator = csDataArray.keys();
                    List<HashMap<String, String>> dayDatakeys = new ArrayList<HashMap<String, String>>();
                    while (iterator.hasNext()) {
                        String key = iterator.next();
                        JSONObject dayDataObject = new JSONObject(csDataArray.optString(key));
                        HashMap<String, String> packageListMap  = new HashMap<String, String>();
                        packageListMap.put("key", key);
                        packageListMap.put("live", dayDataObject.getString("live"));
                        packageListMap.put("password", dayDataObject.getString("password"));
                        dayDatakeys.add(packageListMap);
                        //Log.d("OMIADS", " ======== key : " + key + " = " + dayDataObject.get("live") + " password : " + dayDataObject.getString("password"));
                    }
                    devicePackages  = new DevicePackages(
                            csDataObject.getString("package_id"),
                            csDataObject.getString("week_id"),
                            csDataObject.getString("year_id"),
                            dayDatakeys);
                }
            } else {
                Log.d("OMIADS", " ======= getDevicePackages invalid status : " + responseCode);
            }

            if(os != null) {
                os.close();
            }
            if(is != null) {
                is.close();
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        return devicePackages;
    }

    public DownloadDetails getDownloadDetails(String device_id, String package_id, String day_id, String week_id, String year_id) {
        DownloadDetails         downloadDetails = null;
        int                     responseCode    = 0;
        String                  strResponse     = null;
        HttpURLConnection       connection      = null;
        InputStream             is              = null;
        OutputStream            os              = null;
        try {
            JSONObject obj = new JSONObject();
            obj.put(WebserviceConstant.CMS_DEVICE_ID_KEY, device_id);
            Iterator<String> keys = obj.keys();
            String postStr = "";
            while (keys.hasNext()) {
                if(postStr.length() > 0) {
                    postStr += "&";
                }
                String key = keys.next();
                postStr = postStr + key + "=" + obj.getString(key);
            }
            String mainUrl = CMS_URL + "/devicePackages/" + package_id+ "/days/" + day_id +"/download";
            URL url = new URL(mainUrl);
            connection = (HttpURLConnection) url.openConnection();
            connection.setDoInput(true);
            connection.setRequestProperty (WebserviceConstant.CMS_DEVICE_ID_KEY, device_id);
            connection.setRequestMethod("GET");
            connection.connect();
            responseCode = connection.getResponseCode();
            if(responseCode == HttpURLConnection.HTTP_OK) {
                is = connection.getInputStream();
                if(is != null) {
                    StringBuffer csBuffer = new StringBuffer();
                    InputStreamReader csReader = new InputStreamReader(is);
                    BufferedReader csBufferReader = new BufferedReader(csReader);
                    String out = null;
                    while ((out = csBufferReader.readLine()) != null) {
                        csBuffer.append(out);
                    }
                    strResponse = csBuffer.toString();
                    JSONObject fileDownloadObject = new JSONObject(strResponse);
                    String strUrl = fileDownloadObject.get("url").toString();
                    downloadDetails = new DownloadDetails(strUrl);

                }
            }

            if(os != null) {
                os.close();
            }
            if(is != null) {
                is.close();
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        return downloadDetails;
    }

    public FileData getPassword(String device_id, String package_id, String day_id, String week_id, String year_id, String download_url) {
        FileData                fileData        = null;
        int                     responseCode    = 0;
        Panel                   panel           = null;
        String                  strResponse     = null;
        HttpURLConnection       connection      = null;
        InputStream             is              = null;
        OutputStream            os              = null;
        try {
            JSONObject obj = new JSONObject();
            obj.put(WebserviceConstant.CMS_DEVICE_ID_KEY, device_id);
            Iterator<String> keys = obj.keys();
            String postStr = "";
            while (keys.hasNext()) {
                if(postStr.length() > 0)
                    postStr += "&";
                String key = keys.next();
                postStr = postStr + key + "=" + obj.getString(key);
            }
            String mainUrl = CMS_URL + "/devicePackages/" + package_id+ "/days/" + day_id +"/password";
            //Log.d("OMIADS", " ====== Download :  " + mainUrl);
            URL url = new URL(mainUrl);
            connection = (HttpURLConnection) url.openConnection();
            connection.setDoInput(true);
            connection.setRequestProperty (WebserviceConstant.CMS_DEVICE_ID_KEY, device_id);
            connection.setRequestMethod("GET");
            connection.connect();
            responseCode = connection.getResponseCode();
            if(responseCode == HttpURLConnection.HTTP_OK)
            {
                is = connection.getInputStream();
                if(is != null) {
                    StringBuffer csBuffer = new StringBuffer();
                    InputStreamReader csReader = new InputStreamReader(is);
                    BufferedReader csBufferReader = new BufferedReader(csReader);
                    String out = null;
                    while ((out = csBufferReader.readLine()) != null) {
                        csBuffer.append(out);
                    }
                    strResponse = csBuffer.toString();
                    JSONObject fileDownloadObject = new JSONObject(strResponse);
                    String strPassword = fileDownloadObject.get("password").toString();
                    String filePath  = downloadUrl(day_id, device_id, week_id, year_id, download_url, strPassword);
                    fileData = new FileData("1", filePath, strPassword, day_id, week_id, year_id);
                }
            } else {
                Log.d("OMIADS", " ================= getPassword invalid status : " + responseCode);
            }

            if(os != null) {
                os.close();
            }
            if(is != null) {
                is.close();
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        return fileData;
    }

    private String downloadUrl(String day_id, String device_id, String week_id, String year_id, String strUrl, String password) throws IOException {
        String filepath="";
        try
        {
            URL url = new URL(strUrl);
            HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
            String mainPath = Environment.getExternalStorageDirectory() + "/TricurveAdminPanel/"  + week_id + "_" + year_id + "/" + day_id ;
            String filename= day_id + ".zip";

            File directory = new File(mainPath);

            if(!directory.exists()) {
                directory.mkdirs();
            }
            Log.i("OMIADS"," =============== Package ID : "+mainPath);
            File file = new File(mainPath,filename);
            if(file.createNewFile())
            {
                file.createNewFile();
            }
            FileOutputStream fileOutput = new FileOutputStream(file);
            InputStream inputStream = urlConnection.getInputStream();
            int totalSize = urlConnection.getContentLength();
            int downloadedSize = 0;
            byte[] buffer = new byte[1024];
            int bufferLength = 0;
            while ( (bufferLength = inputStream.read(buffer)) > 0 )
            {
                fileOutput.write(buffer, 0, bufferLength);
                downloadedSize += bufferLength;
                Log.i("Progress:","downloadedSize:"+downloadedSize+"totalSize:"+ totalSize) ;
            }
            fileOutput.close();
            if(downloadedSize==totalSize) filepath=file.getPath();
        }
        catch (MalformedURLException e)
        {
            e.printStackTrace();
        }
        catch (IOException e)
        {
            filepath=null;
            e.printStackTrace();
        }
        Log.i("filepath:"," ============ "+filepath) ;
        return filepath;
    }

    public String turnApi(String device_id, String date, String start, String end, String type) throws IOException {
        String mainUrl = CMS_URL + "/deviceReports";
        String                  strResponse     = null;
        HttpURLConnection       connection      = null;
        InputStream             is              = null;
        OutputStream            os              = null;
        int responseCode;
        try {
            String postStr  = WebserviceConstant.CMS_DEVICE_ID_KEY + "="+device_id + "&date="+date+"&"+"start="+start+"&"+"end="+end+"&"+"type="+type;
            URL url = new URL(mainUrl);
            connection = (HttpURLConnection) url.openConnection();
            connection.setReadTimeout(10000);
            connection.setConnectTimeout(15000);
            connection.setDoOutput(true);
            connection.setDoInput(true);
            connection.setRequestProperty("content-type", "application/x-www-form-urlencoded");
            connection.setRequestProperty("Content-Length", "" + postStr.getBytes().length);
            connection.setRequestMethod("POST");
            connection.connect();
            System.out.println(" ========= mainUrl = " + mainUrl);
            System.out.println(" ========= postStr = " + postStr);
            os = new BufferedOutputStream(connection.getOutputStream());
            os.write(postStr.getBytes());
            os.flush();
            responseCode = connection.getResponseCode();

            if(responseCode == HttpURLConnection.HTTP_OK)
            {
                //do something with response
                is = connection.getInputStream();

                if(is != null) {
                    StringBuffer csBuffer = new StringBuffer();
                    InputStreamReader csReader = new InputStreamReader(is);
                    BufferedReader csBufferReader = new BufferedReader(csReader);

                    String out = null;
                    while ((out = csBufferReader.readLine()) != null) {
                        csBuffer.append(out);
                    }
                    strResponse = csBuffer.toString();
                    System.out.println(" =============== turn Api " + strResponse);
                }
            }
            else
            {
                System.out.println(" =============== Api " + responseCode);
            }

            if(is != null) {
                is.close();
            }

        }
        catch (Exception e)
        {
            e.printStackTrace();
        }

        return "";
    }

    public String turnOnApi(String device_id, String date, String start, String end, String type) throws IOException {
        String mainUrl = CMS_URL + "/deviceReports";
        String                  strResponse     = null;
        HttpURLConnection       connection      = null;
        InputStream             is              = null;
        OutputStream            os              = null;
        int responseCode;
        try {
            String postStr  = WebserviceConstant.CMS_DEVICE_ID_KEY + "="+device_id + "&date="+date+"&"+"start="+start+"&"+"end="+end+"&"+"type="+type;
            URL url = new URL(mainUrl);
            connection = (HttpURLConnection) url.openConnection();
            connection.setDoOutput(true);
            connection.setRequestProperty("start", start);
            connection.setRequestProperty("end", end);
            connection.setRequestProperty("type", type);
            connection.setRequestProperty(WebserviceConstant.CMS_DEVICE_ID_KEY, device_id);
            connection.setRequestMethod("POST");

            System.out.println(" ========= mainUrl = " + mainUrl);
            System.out.println(" ========= postStr = " + postStr);
            responseCode = connection.getResponseCode();

            if(responseCode == HttpURLConnection.HTTP_OK)
            {
                //do something with response
                is = connection.getInputStream();

                if(is != null) {
                    StringBuffer csBuffer = new StringBuffer();
                    InputStreamReader csReader = new InputStreamReader(is);
                    BufferedReader csBufferReader = new BufferedReader(csReader);

                    String out = null;
                    while ((out = csBufferReader.readLine()) != null) {
                        csBuffer.append(out);
                    }
                    strResponse = csBuffer.toString();
                    System.out.println(" =============== turn Api " + strResponse);
                }
            }
            else
            {
                System.out.println(" =============== Api " + responseCode);
            }

            if(is != null) {
                is.close();
            }

        }
        catch (Exception e)
        {
            e.printStackTrace();
        }

        return "";
    }


}
