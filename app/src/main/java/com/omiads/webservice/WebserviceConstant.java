package com.omiads.webservice;

/**
 * Created by kanak on 4/2/17.
 */

public interface WebserviceConstant {
    static final String CMS_URL = "https://api.omiads.in";
    static final String CMS_DEVICE_ID_KEY    = "Device-ID";
}
