package com.omiads.device;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.omiads.controller.FileTransferListener;
import com.omiads.panel.R;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by Kanagaraj on 15-11-2016.
 */
public class DeviceListView extends View implements View.OnTouchListener{

    Bitmap map;
    private Paint paint;
    private ArrayList<Paint> circleList = null;
    Integer[] radiusList = new Integer[] {
            100,
            200,
            300,
            400,
            500,
            600
    };
    private int imageWidth;
    private int imageHeight;
    private Activity activityInfo;
    private int textSize    = 18;
    private List<HashMap<String, String>> connectedDeviceList    = new ArrayList<HashMap<String, String>>();
    private List<HashMap<String, String>> deviceInfo    = new ArrayList<HashMap<String, String>>();
    private Context     csActContext;
    public DeviceListView(Context context) {
        super(context);
        map = Bitmap.createScaledBitmap(BitmapFactory.decodeResource(getResources(), R.drawable.mobile), 40, 40, false) ;
        imageWidth  = map.getWidth();
        imageHeight = map.getHeight();
        paint = new Paint();
        setOnTouchListener(this);
        this.csActContext   = context;

    }

    public DeviceListView(Context context, AttributeSet attrs) {
        super(context, attrs);
        paint = new Paint();
        map = Bitmap.createScaledBitmap(BitmapFactory.decodeResource(getResources(), R.drawable.mobile), 40, 40, false) ;
        imageWidth  = map.getWidth();
        imageHeight = map.getHeight();
        setOnTouchListener(this);
        circleList  = new ArrayList<Paint>();
        this.csActContext   = context;
    }

    public void displayPeerDevices(List<HashMap<String, String>> connectedDeviceList, Activity activityInfo) {
        this.connectedDeviceList  = connectedDeviceList;
        this.activityInfo   = activityInfo;
        invalidate();
    }

    public void addDeviceListPoints(Canvas canvas, int width, int height)
    {
        int circleArea  = 175;
        final int circleNext = 150;
        int index = 0;
        int widthInc = 0;

        while (circleArea < width) {
            if(index == 0) {
                Paint paint = new Paint();
                paint.setColor(Color.WHITE);
                paint.setStyle(Paint.Style.FILL);
                canvas.drawPaint(paint);
                paint.setColor(Color.parseColor("#CD5C5C"));
                canvas.drawCircle(width / 2, height / 2, 75, paint);
                Paint textP = new Paint();
                textP.setTextAlign(Paint.Align.CENTER);
                textP.setTextSize(30);
                textP.setColor(Color.WHITE);
                canvas.drawText("You", (width / 2), (height / 2) + 15, textP);
            } else {
                Paint paint = new Paint();
                paint.setColor(Color.WHITE);
                paint.setStyle(Paint.Style.STROKE);
                canvas.drawPaint(paint);
                paint.setColor(Color.parseColor("#CD5C5C"));
                canvas.drawCircle(width / 2, height / 2, circleArea, paint);
                circleArea += circleNext;
            }
            index++;
        }

        circleArea  = 175;


        deviceInfo.clear();

        for(int dindex = 1; dindex <= connectedDeviceList.size(); dindex++) {
            ImageView textView = new ImageView(csActContext);
            textView.layout(0, 0, imageWidth, imageHeight);
            textView.setDrawingCacheEnabled(true);
            textView.setImageBitmap(map);
            textView.setId(dindex);

            float xpo;
            float ypo;
            Paint circleP = new Paint();

            if(dindex % 2 == 0) {
                xpo = width / 2 + ((float) (circleArea * -Math.cos(index == 1 ? -360 : -290)));
                ypo = height / 2 + ((float) (circleArea * Math.sin(index == 1 ? -360 : -290)));
                circleP.setColor(Color.RED);
                canvas.drawCircle(xpo, ypo, 40, circleP);
                HashMap<String, String> device  = new HashMap<String, String>();
                device.put("x", "" + (xpo - (imageWidth / 2)));
                device.put("y", "" + (ypo - (imageHeight / 2)));
                deviceInfo.add(device);
                canvas.drawBitmap(textView.getDrawingCache(), xpo - (imageWidth / 2), ypo - (imageHeight / 2), null);
                circleP.setColor(Color.BLACK);
                circleP.setTextSize(textSize);
                canvas.drawText("" + connectedDeviceList.get(dindex-1).get("macaddress").toUpperCase(), xpo - (imageWidth / 2), ypo - (imageHeight / 2), circleP);
                canvas.drawText("" + connectedDeviceList.get(dindex-1).get("macaddress").toUpperCase(), xpo - (imageWidth / 2), (textSize + ypo) - (imageHeight / 2), circleP);
                circleArea += circleNext;
            } else {
                xpo = width / 2 + ((float) (circleArea * -Math.cos(index == 1 ? 45 : 90)));
                ypo = height / 2 + ((float) (circleArea * Math.sin(index == 1 ? 45 : 90)));
                circleP.setColor(Color.GREEN);
                circleP.setStyle(Paint.Style.FILL);
                canvas.drawCircle(xpo, ypo, 40, circleP);
                HashMap<String, String> device  = new HashMap<String, String>();
                device.put("x", "" + (xpo - (imageWidth / 2)));
                device.put("y", "" + (ypo - (imageHeight / 2)));
                deviceInfo.add(device);
                canvas.drawBitmap(textView.getDrawingCache(), xpo - (imageWidth / 2), ypo - (imageHeight / 2), null);
                TextView textView2  = new TextView(csActContext);
                textView2.setText("" + connectedDeviceList.get(dindex-1).get("macaddress").toUpperCase());
                textView2.setDrawingCacheEnabled(true);
                circleP.setColor(Color.BLACK);
                circleP.setTextSize(textSize);
                canvas.drawText("" + connectedDeviceList.get(dindex-1).get("macaddress").toUpperCase(), xpo - (imageWidth / 2), ypo - (imageHeight / 2), circleP);
                canvas.drawText("" + connectedDeviceList.get(dindex-1).get("macaddress").toUpperCase(), xpo - (imageWidth / 2), (textSize + ypo) - (imageHeight / 2), circleP);
            }
        }

    }

    @Override
    protected void onDraw(Canvas canvas) {
        int x = getWidth();
        int y = getHeight();
        addDeviceListPoints(canvas, x, y);
        super.onDraw(canvas);
    }

    boolean bIsDeviceListed = false;
    int selectedDevice = -1;
    @Override
    public boolean onTouch(View v, MotionEvent event) {
        float x = event.getX();
        float y = event.getY();
        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN:
                for (int index = 0; index < deviceInfo.size(); index++) {
                    float deviceX = Float.parseFloat(deviceInfo.get(index).get("x"));
                    float deviceY = Float.parseFloat(deviceInfo.get(index).get("y"));
                    if (deviceX < x
                            && deviceY < y
                            && x < (deviceX + imageWidth)
                            && y < (deviceY + imageHeight)) {
                        selectedDevice = index;
                        invalidate();
                        Log.d("CLIENT", " =====> Connection Called");
                        ((FileTransferListener)this.activityInfo).triggerClient(connectedDeviceList.get(selectedDevice).get("ipaddress"), bIsDeviceListed);
                    }
                }
                break;
            default:
                break;
        }
        return false;
    }

    public void deviceConnected() {
        bIsDeviceListed = true;
    }

    public void deviceFailedToConnect() {
        bIsDeviceListed = false;
    }

    public void fileShareDone() {
        bIsDeviceListed = false;
    }
}
