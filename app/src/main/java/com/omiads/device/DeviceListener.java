package com.omiads.device;

/**
 * Created by kanak on 3/1/17.
 */

public interface DeviceListener {
    void onDeviceConnected(DeviceState deviceState);
}
