package com.omiads.device;

import android.os.AsyncTask;
import android.util.Log;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.Socket;
import java.net.UnknownHostException;

public class DeviceClient extends AsyncTask<Void, Void, Void> {

	private File 				csFiles 			= null;
	private DataOutputStream 	dataOutputStream 	= null;
	private DataInputStream 	dataInputStream 	= null;
	private DeviceListener 		listener			= null;
	private DeviceState			csDeviceState		= null;
	private String 				dstAddress			= "";
	private String				textResponse		= "";
	private int					dstPort;

	public DeviceClient(String addr, int port, String textResponse, DeviceListener clientConnectListener) {
		this.csDeviceState	= DeviceState.IDLE;
		this.dstAddress = addr;
		this.dstPort = port;
		this.textResponse=textResponse;
		this.listener = clientConnectListener;
		this.csFiles = null;
	}

	public DeviceClient(String addr, int port, File files, DeviceListener clientConnectListener) {
		this.dstAddress = addr;
		this.dstPort = port;
		this.csFiles = files;
		this.listener = clientConnectListener;
		this.textResponse = null;
	}

	@Override
	protected Void doInBackground(Void... arg0) {
		Socket socket = null;
		try {
			socket = new Socket(dstAddress, dstPort);
			dataOutputStream = new DataOutputStream(socket.getOutputStream());
			if(csFiles != null) {
				Log.d("OMIADS", "======> File path to write = " + csFiles.getAbsolutePath());
				FileInputStream fileStream = new FileInputStream(csFiles);
				byte[] buffer = new byte[1024];
				int read = -1;
				while ((read = fileStream.read(buffer)) != -1){
					dataOutputStream.write(buffer, 0, read);
				}
				csDeviceState	= DeviceState.SUCCESS;

			}
			if (textResponse != null) {
				Log.d("CLIENT", " ======> Device list received from server = " + textResponse);
				dataOutputStream.write(textResponse.getBytes());
				csDeviceState	= DeviceState.CONNECTED;
			}
		} catch (UnknownHostException e) {
			csDeviceState	= DeviceState.FAILED;
		} catch (IOException e) {
			csDeviceState	= DeviceState.FAILED;
		} finally {
			try {
				if (dataOutputStream != null){
					dataOutputStream.close();
				}
				if (dataInputStream != null){
					dataInputStream.close();
				}
				if (socket != null) {
					socket.close();
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return null;
	}

	@Override
	protected void onPostExecute(Void result) {
		if(listener != null) {
			listener.onDeviceConnected(csDeviceState);
		}
		super.onPostExecute(result);
	}

}
