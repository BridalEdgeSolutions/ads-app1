package com.omiads.device;

/**
 * Created by kanak on 3/1/17.
 */

public enum DeviceState {
    IDLE,
    CONNECTED,
    FAILED,
    SUCCESS
}
