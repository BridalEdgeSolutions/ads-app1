package com.omiads.model;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by kanak on 12/2/17.
 */

public class SlotList {
    String slot_id;
    ArrayList<HashMap<String, String>>  slotImageList;

    public SlotList(String slot_id, ArrayList<HashMap<String, String>>  slotImageList) {
        this.slot_id    = slot_id;
        this.slotImageList  = slotImageList;
    }

    public String getSlotId() {
        return this.slot_id;
    }

    public ArrayList<HashMap<String, String>> getSlotAds() {
        return this.slotImageList;
    }

}
