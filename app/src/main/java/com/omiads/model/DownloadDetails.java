package com.omiads.model;

/**
 * Created by kanak on 8/2/17.
 */

public class DownloadDetails {
    String url;
    public DownloadDetails(String url) {
        this.url = url;
    }

    public String getUrl() {
        return url;
    }
}
