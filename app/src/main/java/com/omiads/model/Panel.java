package com.omiads.model;

/**
 * Created by kanak on 4/2/17.
 */

public class Panel {
    String device_id;
    String status;

    public Panel(String status, String device_id) {
        this.device_id   = device_id;
        this.status = status;
    }

    public void setDeviceId(String device_id) {
        this.device_id  = device_id;
    }

    public String getDeviceId() {
        return device_id;
    }

    public void setStatus(String status) {
        this.status  = status;
    }

    public String getStatus() {
        return status;
    }
}
