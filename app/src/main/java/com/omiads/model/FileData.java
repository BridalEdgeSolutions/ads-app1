package com.omiads.model;

import java.io.File;

/**
 * Created by kanak on 8/2/17.
 */

public class FileData {
    String file_path;
    String status;
    String password;
    String package_id;
    String week_id;
    String year_id;

    public FileData(String status, String file_path, String password, String package_id, String week_id, String year_id) {
        this.file_path  = file_path;
        this.status     = status;
        this.password   = password;
        this.package_id = package_id;
        this.week_id    = week_id;
        this.year_id    = year_id;
    }

    public String getStatus() {
        return this.status;
    }

    public String getFilePath() {
        return this.file_path;
    }

    public String getPassword() {
        return this.password;
    }

    public String getPackageId() {
        return this.package_id;
    }

    public String getWeekId() {
        return this.week_id;
    }

    public String getYearId() {
        return this.year_id;
    }
}
