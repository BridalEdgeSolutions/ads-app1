package com.omiads.model;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by kanak on 9/2/17.
 */

public class AdsData {
    private HashMap<String,  ArrayList<HashMap<String, String>>> slotsid;
    private ArrayList<String> slotid;
    private String  package_id;
    private String  week_id;
    private String  year_id;
    public AdsData(ArrayList<String> slotid, HashMap<String,  ArrayList<HashMap<String, String>>> slotsid, String package_id) {
        this.slotsid    = slotsid;
        this.slotid     = slotid;
        this.package_id = package_id;
        this.week_id    = week_id;
        this.year_id    = year_id;
    }

    public ArrayList<String> getSlotIds() {
        return this.slotid;
    }

    public HashMap<String,  ArrayList<HashMap<String, String>>> getSlotsList() {
        return this.slotsid;
    }

    public String getPackageId() {
        return this.package_id;
    }

    public String getWeekId() {
        return this.week_id;
    }

    public String getYearId() {
        return this.year_id;
    }
}
