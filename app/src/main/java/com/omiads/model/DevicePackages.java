package com.omiads.model;

import java.util.HashMap;
import java.util.List;

/**
 * Created by kanak on 8/2/17.
 */

public class DevicePackages {
    String device_id;
    String package_id;
    String week_id;
    String year_id;
    List<HashMap<String, String>> packagesList;

    public DevicePackages(String package_id, String week_id, String year_id, List<HashMap<String, String>> packagesList) {
        this.package_id     = package_id;
        this.week_id        = week_id;
        this.year_id        = year_id;
        this.packagesList   = packagesList;

    }

    public String getDeviceId() {
        return device_id;
    }

    public String getPackageId() {
        return package_id;
    }

    public String getWeekId() {
        return week_id;
    }

    public String getYearId() {
        return year_id;
    }

    public List<HashMap<String, String>> getPackageList() {
        return packagesList;
    }

}
