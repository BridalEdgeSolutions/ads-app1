package com.omiads.model;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by kanak on 9/2/17.
 */

public class AdsUtility {
    public static AdsUtility adsUtility;
    public static AdsUtility getInstance() {
        if(adsUtility == null) {
            adsUtility  = new AdsUtility();
        }
        return adsUtility;
    }

    public AdsData getAdsRunnableList(String path, String package_id) {
        AdsData adsData = null;
        File adsJsonFile    = new File(path);

        if(adsJsonFile.exists()) {
            try {
                BufferedReader adsInputStream  = new BufferedReader(new FileReader(adsJsonFile));
                String strDataJson = convertStreamToString(adsInputStream);
                System.out.println( " ======= strDataJson " + strDataJson);
                JSONArray jsonArray = new JSONArray(strDataJson);
                HashMap<String,  ArrayList<HashMap<String, String>>> indexs  = new HashMap<String,  ArrayList<HashMap<String, String>>>();
                if(jsonArray != null) {
                    ArrayList<String> slotids   = new ArrayList<String>();
                    for (int index = 0; index < jsonArray.length(); index++) {
                        JSONObject jsonObject   = jsonArray.getJSONObject(index);
                        slotids.add(jsonObject.getString("slot_id"));
                        JSONArray   slidesArray = jsonObject.getJSONArray("slides");
                        ArrayList<HashMap<String, String>> slotImages   = new ArrayList<HashMap<String, String>>();
                        for (int position = 0; position < slidesArray.length(); position++) {
                            JSONObject  slotImageObject = slidesArray.getJSONObject(position);
                            HashMap<String, String> images  = new HashMap<String, String>();
                            images.put("slide_id", slotImageObject.getString("slide_id"));
                            images.put("img", slotImageObject.getString("img"));
                            slotImages.add(images);
                        }
                        indexs.put("" + jsonObject.getString("slot_id"), slotImages);
                        System.out.println( " ===================23 " + jsonObject.getString("slot_id"));
                        System.out.println( " ========== SLIDES " + jsonObject.get("slides"));
                    }
                    adsData = new AdsData(slotids, indexs, package_id);
                }
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return adsData;
    }

    public String convertStreamToString(BufferedReader reader) {
        StringBuilder sb = new StringBuilder();
        String line;
        try {
            while ((line = reader.readLine()) != null) {
                sb.append(line).append('\n');
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        return sb.toString();
    }
}
