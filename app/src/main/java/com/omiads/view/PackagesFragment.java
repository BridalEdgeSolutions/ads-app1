package com.omiads.view;


import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;

import com.baoyz.swipemenulistview.SwipeMenu;
import com.baoyz.swipemenulistview.SwipeMenuCreator;
import com.baoyz.swipemenulistview.SwipeMenuItem;
import com.baoyz.swipemenulistview.SwipeMenuListView;
import com.omiads.panel.R;

public class PackagesFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    public boolean bIsPackages = false;

    SwipeMenuListView   csListView              = null;
    View                csMainView              = null;
    MyListAdapter       csAdapter               = null;

    public PackagesFragment() {
        // Required empty public constructor
    }
    public static PackagesFragment newInstance(String param1, String param2) {
        PackagesFragment fragment = new PackagesFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        csMainView = inflater.inflate(R.layout.fragment_packages, container, false);

        getFragmentResources();

        return csMainView;
    }



    private void getFragmentResources()
    {
        csListView = (SwipeMenuListView) csMainView.findViewById(R.id.packages_swipeMenuList);
        createSwipeMenu();

        csAdapter    = new MyListAdapter();
        csListView.setAdapter(csAdapter);
    }


    private void createSwipeMenu()
    {
        SwipeMenuCreator creator = new SwipeMenuCreator() {
            @Override
            public void create(SwipeMenu menu) {

                // create "delete" item
                SwipeMenuItem viewItem1 = new SwipeMenuItem(getActivity());
                // set item background
                viewItem1.setBackground(R.drawable.menu_bg);
                viewItem1.setIcon(R.drawable.trash);
                // set item width
                viewItem1.setWidth(150);
                // add to menu
                menu.addMenuItem(viewItem1);

            }
        };
        // set creator
        csListView.setMenuCreator(creator);
        csListView.setSwipeDirection(SwipeMenuListView.DIRECTION_LEFT);
        csListView.setOnMenuItemClickListener(listMenuClickListener);
        csListView.setOnItemClickListener(itemClick);
    }

    public SwipeMenuListView.OnMenuItemClickListener listMenuClickListener = new SwipeMenuListView.OnMenuItemClickListener() {
        @Override
        public boolean onMenuItemClick(int position, SwipeMenu menu, int index) {

            switch (index)
            {
                case 0:
                {
                    //openTabFragment(3);
                    break;
                }
            }
            return false;
        }
    };

    public AdapterView.OnItemClickListener itemClick = new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

            //((CompanyActivity)getActivity()).moveToWifiActivity();
        }
    };

    private class MyListAdapter extends BaseAdapter {


        @Override
        public int getCount() {
            int count = 5;// csListJsonArr != null ? csListJsonArr.length() : 0;
            return count;
        }

        @Override
        public Object getItem(int position) {
            return null;
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        private class Holder
        {
            ImageView   csDownloadImg       = null;
            ImageView   csDeleteImg         = null;
            TextView    csTitleTxt          = null;
            TextView    csStatusTxt         = null;
            TextView    csDateTxt           = null;
            SeekBar     csSeekBar           = null;

            RelativeLayout csRow1Lay        = null;
            RelativeLayout csRow2Lay        = null;
            RelativeLayout csRow3Lay        = null;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {

            Holder csHolder = null;

            if (convertView != null) {
                csHolder = (Holder) convertView.getTag();
            } else {
                csHolder = new Holder();
                LayoutInflater inflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                convertView = inflater.inflate(R.layout.packages_design, null, false);

                csHolder.csTitleTxt     = (TextView) convertView.findViewById(R.id.packagesDesign_titleText);
                csHolder.csDateTxt      = (TextView) convertView.findViewById(R.id.packagesDesign_dateText);
                csHolder.csStatusTxt    = (TextView) convertView.findViewById(R.id.packagesDesign_statusTxt);
                csHolder.csDeleteImg    = (ImageView) convertView.findViewById(R.id.packagesDesign_deleteIcon);
                csHolder.csDownloadImg  = (ImageView) convertView.findViewById(R.id.packagesDesign_DownloadIcon);
                csHolder.csSeekBar      = (SeekBar) convertView.findViewById(R.id.packagesDesign_seekBar);

                csHolder.csRow1Lay      = (RelativeLayout) convertView.findViewById(R.id.packagesDesign_row1);
                csHolder.csRow2Lay      = (RelativeLayout) convertView.findViewById(R.id.packagesDesign_row2);
                csHolder.csRow3Lay      = (RelativeLayout) convertView.findViewById(R.id.packagesDesign_row3);

                convertView.setTag(csHolder);
            }

            csHolder.csSeekBar.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View view, MotionEvent motionEvent) {
                    return true;
                }
            });

            csHolder.csDownloadImg.setVisibility(View.INVISIBLE);
            csHolder.csRow3Lay.setVisibility(View.GONE);

            if(bIsPackages == false)
            {
                if(position == 2)
                {
                    csHolder.csDownloadImg.setVisibility(View.INVISIBLE);
                    csHolder.csRow3Lay.setVisibility(View.VISIBLE);
                }
                else
                {
                    csHolder.csDownloadImg.setVisibility(View.VISIBLE);
                    csHolder.csRow3Lay.setVisibility(View.GONE);
                }
            }

            return convertView;
        }

    }
}
