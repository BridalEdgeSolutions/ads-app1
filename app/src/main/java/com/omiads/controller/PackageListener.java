package com.omiads.controller;

import com.omiads.model.AdsData;

import java.util.ArrayList;

/**
 * Created by kanak on 12/2/17.
 */

public interface PackageListener {
    public void onPackageDownloaded(ArrayList<AdsData> adsDatas, String device_id);
}
