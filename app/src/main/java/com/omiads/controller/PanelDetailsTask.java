package com.omiads.controller;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Environment;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.widget.Toast;

import com.omiads.model.AdsData;
import com.omiads.model.AdsUtility;
import com.omiads.model.DevicePackages;
import com.omiads.model.DownloadDetails;
import com.omiads.model.FileData;
import com.omiads.model.Panel;
import com.omiads.panel.FileRcdListener;
import com.omiads.webservice.Webservice;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by kanak on 12/2/17.
 */


public class PanelDetailsTask extends AsyncTask<Void, Void, Panel> {
    private Context context;
    private String device_id;
    private Panel  devicePanel;
    private DevicePackages  devicePackages;
    private ArrayList<AdsData> adsData;
    ArrayList<HashMap<String, String>> passwordList;
    private PackageListener packageListener;
    private FileRcdListener fileRcdListener;
    public PanelDetailsTask(Context context, PackageListener packageListener, FileRcdListener fileRcdListener) {
        this.context    = context;
        this.packageListener    = packageListener;
        this.fileRcdListener    = fileRcdListener;
    }

    @Override
    protected Panel doInBackground(Void... params) {
        passwordList    = new ArrayList<HashMap<String, String>>();
        TelephonyManager telephonyManager = (TelephonyManager) this.context.getSystemService(Context.TELEPHONY_SERVICE);
        devicePanel = Webservice.getInstance().getPanels(telephonyManager.getDeviceId());
        Log.d("OMIADS", " ====== Telephony Device Id : " + telephonyManager.getDeviceId());
        devicePackages = Webservice.getInstance().getDevicePackages(devicePanel.getDeviceId());
        if(devicePackages != null) {
            if(devicePackages.getPackageList().size() > 0) {
                for (int index = 0; index < devicePackages.getPackageList().size();index++) {
                    DownloadDetails downloadDetails = Webservice.getInstance().getDownloadDetails(
                            devicePanel.getDeviceId(),
                            devicePackages.getPackageId(),
                            devicePackages.getPackageList().get(index).get("key"),
                            devicePackages.getWeekId(),
                            devicePackages.getYearId());
                    if(downloadDetails != null && downloadDetails.getUrl() != null) {

                        FileData fileData = Webservice.getInstance().getPassword(
                                devicePanel.getDeviceId(),
                                devicePackages.getPackageId(),
                                devicePackages.getPackageList().get(index).get("key"),
                                devicePackages.getWeekId(),
                                devicePackages.getYearId(),
                                downloadDetails.getUrl());
                        if (fileData != null) {
                            //fileLocationPath = fileData.getFilePath();
                            //password = fileData.getPassword();
                            HashMap<String, String> detailsList = new HashMap<String, String>();
                            detailsList.put("password", fileData.getPassword());
                            detailsList.put("path", fileData.getFilePath());
                            detailsList.put("key", fileData.getPackageId());
                            detailsList.put("week_id", fileData.getWeekId());
                            detailsList.put("year_id", fileData.getYearId());
                            passwordList.add(detailsList);
                        }
                    }
                }

            }

        } else {
            Log.d("OMIADS", " ======== Package Id failed");
        }


        return devicePanel;
    }

    @Override
    protected void onPostExecute(Panel panel) {
        if(panel != null && panel.getStatus() != null && panel.getStatus().equals("1")) {
            adsData = new ArrayList<AdsData>();
            Toast.makeText(context, "File Downloaded successfully...", Toast.LENGTH_LONG).show();
            //Environment.getExternalStorageDirectory() + "/TricurveAdminPanel/fb.zip"
            for (int index = 0; index < passwordList.size(); index++) {
                ZipExtractor csUnZip = new ZipExtractor(context,
                        passwordList.get(index).get("path"),
                        Environment.getExternalStorageDirectory() + "/TricurveAdminPanel/" +  passwordList.get(index).get("week_id") + "_" + passwordList.get(index).get("year_id") + "/" +  passwordList.get(index).get("key"),
                        fileRcdListener,
                        passwordList.get(index).get("password"));
                csUnZip.execute();
                AdsData ads = AdsUtility.getInstance().getAdsRunnableList(
                        Environment.getExternalStorageDirectory() + "/TricurveAdminPanel/" +  passwordList.get(index).get("week_id") + "_" + passwordList.get(index).get("year_id") +  "/" + passwordList.get(index).get("key") + "/data.json",
                        passwordList.get(index).get("key"));
                adsData.add(ads);
            }
            packageListener.onPackageDownloaded(adsData, devicePanel.getDeviceId());
        } else {
            Toast.makeText(context, "Package download failed...", Toast.LENGTH_LONG).show();
        }
        super.onPostExecute(panel);
    }
}
