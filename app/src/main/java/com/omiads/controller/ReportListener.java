package com.omiads.controller;

/**
 * Created by kanak on 12/2/17.
 */

public interface ReportListener {
    public void onReportUpdated();
}
