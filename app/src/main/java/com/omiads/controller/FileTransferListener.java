package com.omiads.controller;

/**
 * Created by kanak on 21/12/16.
 */

public interface FileTransferListener {
    void sharedDone();
    void triggerClient(String ipAddress, boolean isFiles);
}
