package com.omiads.controller;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.omiads.panel.FileRcdListener;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;
import java.util.zip.ZipInputStream;

/**
 * Created by Kanagaraj on 03-01-2017.
 */
public class ZipExtractor extends AsyncTask<Void, Void, Void>
{
    private String strZip               = null;
    private String password               = null;
    private String strTargetLocation    = null;
    private Context csContext           = null;
    //private ProgressDialog csDialog     = null;
    private FileRcdListener listener    = null;


    public ZipExtractor(Context ctx, String zipFile, String targetLocation, FileRcdListener listener, String password)
    {
        csContext               = ctx;
        strZip                  = zipFile;
        strTargetLocation       = targetLocation;
        this.listener           = listener;
        this.password           = password;
    }



    public void unzip() {
        if(strZip != null && strTargetLocation != null) {
            File target = new File(strTargetLocation);
            if(!target.exists())
                target.mkdir();

            InputStream is;
            ZipInputStream zis;
            try {
                String filename;
                is = new FileInputStream(strZip);

                zis = new ZipInputStream(new BufferedInputStream(is));
                ZipEntry ze;
                byte[] buffer = new byte[1024];
                int count;

                while ((ze = zis.getNextEntry()) != null)
                {
                    filename = ze.getName();
                    Log.d("=====>", "zip filename : " + filename);
                    File fmd = new File(strTargetLocation + "/" + filename);
                    if(fmd.exists())
                        fmd.delete();

                    checkDirectories(fmd.getAbsolutePath());
                    FileOutputStream fout = new FileOutputStream(strTargetLocation + "/" + filename);

                    while ((count = zis.read(buffer)) != -1)
                    {
                        fout.write(buffer, 0, count);
                    }

                    fout.close();
                    zis.closeEntry();
                }

                zis.close();
            }
            catch(Exception e)
            {

                e.printStackTrace();
            }
        }
    }

    public void unzipWithPassword() throws IOException {
        if(strZip != null && strTargetLocation != null)
        {
            File target = new File(strTargetLocation);
            if(!target.exists()) {
                target.mkdir();
            }

            InputStream is;
            ZipInputStream zis;
            try
            {
                net.lingala.zip4j.core.ZipFile zipFile  = new net.lingala.zip4j.core.ZipFile(strZip);
                if(zipFile.isEncrypted()) {
                    zipFile.setPassword(password);
                }
                zipFile.extractAll(target.getAbsolutePath());
                Log.d("Files", " ================= Path: " + target.getAbsolutePath());
                File directory = new File(target.getAbsolutePath());
                if(!directory.exists()) {
                    directory.mkdirs();
                }
                File[] files = directory.listFiles();
                Log.d("Files", "Size: "+ files.length);
                String zipPath = "";
                for (int i = 0; i < files.length; i++)
                {
                    zipPath = files[i].getName();
                }

                net.lingala.zip4j.core.ZipFile nestedZipFile  = new net.lingala.zip4j.core.ZipFile(target.getAbsolutePath() + "/" + zipPath);
                File pathFile   = new File(target.getAbsolutePath());
                if(!pathFile.exists()) {
                    pathFile.mkdir();
                }
                nestedZipFile.extractAll(target.getAbsolutePath());
            }
            catch(Exception e)
            {
                System.out.println(" ============ " + e.getMessage());
                e.printStackTrace();
            }
        }
    }

    public void checkDirectories(String path) {

        String[] pathLists = path.split("/");

        String checkPath = "";

        for (String sPath : pathLists) {

            if(sPath.contains(".") == false){

                checkPath = checkPath + "/"  + sPath;
                File newFile = new File(checkPath);
                if(newFile.exists() == false){
                    newFile.mkdir();
                }
            }
        }

    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        if(csContext != null) {
            //csDialog = new ProgressDialog(csContext);
            //csDialog.setMessage("Please wait until unzipping...");
            //csDialog.show();
        }
        if (listener != null) {
            listener.onFileDownloading();
        }
    }

    @Override
    protected Void doInBackground(Void... params)
    {

        if(password!= null && !password.equals("")) {
            try {
                unzipWithPassword();
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else {
            unzip();
        }
        return null;
    }

    @Override
    protected void onPostExecute(Void aVoid) {
        super.onPostExecute(aVoid);
        removeDialog("removed from post execute");

        /*if(strZip != null) {
            File zipFile = new File(strZip);
            if(zipFile.exists())
                zipFile.delete();
        }*/

        if(listener!=null) {
            listener.onFileReceived();
        }
    }

    @Override
    protected void onCancelled() {
        super.onCancelled();
        removeDialog("remove from cancelled");
    }

    @Override
    protected void onCancelled(Void aVoid) {
        super.onCancelled(aVoid);
        removeDialog("removed from cancelled two");
    }

    private void removeDialog(String from)
    {
        Log.d("", "======  " + from);

        //if(csDialog != null && csDialog.isShowing())
        //    csDialog.dismiss();

        //csDialog = null;
    }
}
