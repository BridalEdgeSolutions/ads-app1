package com.omiads.controller;

import android.content.Context;
import android.os.AsyncTask;
import android.telephony.TelephonyManager;

import com.omiads.webservice.Webservice;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by kanak on 12/2/17.
 */

public class ReportCollectionTask extends AsyncTask<Void, Void, Void> {

    private Context context;
    private ReportListener reportListener;
    private String starttime, endtime, type;
    private String device_id;
    public ReportCollectionTask(Context context, String device_id, String starttime, String endtime, String type, ReportListener reportListener) {
        this.context        = context;
        this.reportListener = reportListener;
        this.starttime      = starttime;
        this.endtime        = endtime;
        this.type           = type;
        this.device_id      = device_id;
    }

    @Override
    protected Void doInBackground(Void... params) {
        TelephonyManager telephonyManager = (TelephonyManager) this.context.getSystemService(Context.TELEPHONY_SERVICE);
        SimpleDateFormat sdf = new SimpleDateFormat ("dd-MM-yyyy");
        String date = sdf.format(new Date());
        try {
            Webservice.getInstance().turnApi(device_id, date, starttime, endtime, type);
            Webservice.getInstance().turnOnApi(device_id, date, starttime, endtime, type);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    protected void onPostExecute(Void aVoid) {
        reportListener.onReportUpdated();
        super.onPostExecute(aVoid);
    }
}
