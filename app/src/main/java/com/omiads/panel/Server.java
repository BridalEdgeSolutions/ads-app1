package com.omiads.panel;

import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.util.Log;

import com.omiads.controller.ImageDisplayListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintStream;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketException;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;

public class Server {
	WifiListActivity activity;
	ServerSocket serverSocket;
	String message = "";
	static final int socketServerPORT = 8080;
	private ServerListener serverListener;
	private ImageDisplayListener imageDisplayListener;
	public Server(WifiListActivity activity, ServerListener listener, ImageDisplayListener imageDisplayListener) {
		this.activity = activity;
		this.serverListener = listener;
		this.imageDisplayListener	= imageDisplayListener;
		Thread socketServerThread = new Thread(new SocketServerThread());
		socketServerThread.start();
	}

	public int getPort() {
		return socketServerPORT;
	}

	public void onDestroy() {
		if (serverSocket != null) {
			try {
				serverSocket.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}


	public class FileHandler extends Handler {
		@Override
		public void handleMessage(Message msg) {
			if(msg.what == 11) {
				imageDisplayListener.showAds(strPath,zip_target, day_id, zip_password);
			}
			super.handleMessage(msg);
		}

	}

	private String strPath	= "";
	String zip_password		= "";
	String zip_target		= "";
	String day_id	= "";

	private FileHandler fileHandler	= new FileHandler();
	private class SocketServerThread extends Thread {

		int count = 0;

		@Override
		public void run() {
			DataInputStream dataInputStream = null;
			BufferedInputStream	bufferedInputStream	= null;

			try {
				serverSocket = new ServerSocket(socketServerPORT);
				String response ="";

				while (true) {
					Socket socket = serverSocket.accept();
					count++;
					message += "#" + count + " from "
							+ socket.getInetAddress() + ":"
							+ socket.getPort() + "\n";
					int bytesRead;


					Log.d("SERVER", "======> filesize in aserver before socket");
					Log.d("SERVER","======> filesize in aserver before socket");


					bufferedInputStream	= new BufferedInputStream(socket.getInputStream());
					long filesSize = bufferedInputStream.available();
					Log.d("SERVER","======> File size : " + filesSize);
					dataInputStream = new DataInputStream(bufferedInputStream);


					if(filesSize > (1024) || filesSize == 0)
					{
						String mainPath = Environment.getExternalStorageDirectory() + "/TricurveAdminPanel";
						Log.d("SERVER","======> Directory Path : " + mainPath);
						File mainFile = new File(mainPath);
						if(!mainFile.mkdir()) {
								Log.d("SERVER","======> Directory Not Created!!!");
						}

						int read = -1;
						byte[] buffer = new byte[1024];
						String getPath = ".zip";

						String info_data	= dataInputStream.readUTF().toString();
						Log.d("SERVER","======> File Creating......!!!" + info_data);

						String week_id	= "";
						String year_id	= "";


						try {
							JSONObject	jsonObject	= new JSONObject(info_data);
							day_id	= jsonObject.getString("day_id");
							week_id	= jsonObject.getString("week_id");
							year_id	= jsonObject.getString("year_id");
							zip_password	= jsonObject.getString("password");


						} catch (JSONException e) {
							e.printStackTrace();
						}
						String srcPath	= mainFile + "/" + week_id + "_" + year_id + "/" + day_id;
						File srcPathFile = new File(srcPath);
						if(!srcPathFile.exists()) {
							srcPathFile.mkdirs();
						}
						zip_target	= srcPathFile.getAbsolutePath();

								File checkFile = new File(srcPath, (day_id + getPath));

						if(checkFile.exists())
							checkFile.delete();
						//Log.d("SERVER","======> File Creating......!!!" + dataInputStream.readUTF());
						if(!checkFile.exists()) {


							FileOutputStream fileOut = new FileOutputStream(checkFile.getAbsolutePath());
							while ((read = dataInputStream.read(buffer)) != -1) {
								fileOut.write(buffer, 0, read);
							}
							strPath = checkFile.getAbsolutePath();
							fileHandler.sendEmptyMessage(11);
						} else {
							Log.d("SERVER","======> File Exists!!!");
							strPath = checkFile.getAbsolutePath();
							fileHandler.sendEmptyMessage(11);
						}
					}
					else
					{

						Log.d("SERVER","======> filesize not in aserver socket : 0");

						BufferedReader r = new BufferedReader(new InputStreamReader(socket.getInputStream()));
						StringBuilder total = new StringBuilder();
						String line;
						while ((line = r.readLine()) != null) {
							total.append(line).append('\n');
						}
						Log.d("SERVER"," ======> server " + total);
						JSONObject	dataObject	= null;

						List<HashMap<String, String>> connectedDeviceList    = new ArrayList<HashMap<String, String>>();

						try {
							dataObject	= new JSONObject(total.toString());
							JSONArray dataArray	= dataObject.getJSONArray("data");

							for (int index = 0; index < dataArray.length(); index++) {
								JSONObject deviceData = dataArray.getJSONObject(index);
								HashMap<String, String> map = new HashMap<String, String>();
								map.put("ipaddress", deviceData.getString("ipaddress"));
								map.put("macaddress", deviceData.getString("macaddress"));
								connectedDeviceList.add(map);
							}
						} catch (JSONException e) {
							e.printStackTrace();
						}
						serverListener.sendDeviceList(connectedDeviceList);

						SocketServerReplyThread socketServerReplyThread = new SocketServerReplyThread(
								socket, count);
						socketServerReplyThread.run();
					}
				}
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} finally {
				if (dataInputStream != null){
					try {
						dataInputStream.close();
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			}
		}

	}

	private class SocketServerReplyThread extends Thread {

		private Socket hostThreadSocket;
		int cnt;

		SocketServerReplyThread(Socket socket, int c) {
			hostThreadSocket = socket;
			cnt = c;
		}

		@Override
		public void run() {
			OutputStream outputStream;
			String msgReply = "SUCCESS";

			try {
				outputStream = hostThreadSocket.getOutputStream();
				PrintStream printStream = new PrintStream(outputStream);
				printStream.print(msgReply);
				Log.d("SERVER", " ======> msgreply");
				printStream.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

	}

	public String getIpAddress() {
		String ip = "";
		try {
			Enumeration<NetworkInterface> enumNetworkInterfaces = NetworkInterface
					.getNetworkInterfaces();
			while (enumNetworkInterfaces.hasMoreElements()) {
				NetworkInterface networkInterface = enumNetworkInterfaces
						.nextElement();
				Enumeration<InetAddress> enumInetAddress = networkInterface
						.getInetAddresses();
				while (enumInetAddress.hasMoreElements()) {
					InetAddress inetAddress = enumInetAddress
							.nextElement();

					if (inetAddress.isSiteLocalAddress()) {
						ip += "Server running at : "
								+ inetAddress.getHostAddress();
					}
				}
			}

		} catch (SocketException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			ip += "Something Wrong! " + e.toString() + "\n";
		}
		return ip;
	}
}
