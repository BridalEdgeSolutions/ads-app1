package com.omiads.panel;

import android.Manifest;
import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.net.wifi.ScanResult;
import android.net.wifi.SupplicantState;
import android.net.wifi.WifiConfiguration;
import android.net.wifi.WifiManager;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.PowerManager;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.omiads.controller.FileTransferListener;
import com.omiads.controller.ImageDisplayListener;
import com.omiads.controller.PackageListener;
import com.omiads.controller.PanelDetailsTask;
import com.omiads.controller.ReportCollectionTask;
import com.omiads.controller.ReportListener;
import com.omiads.controller.ZipExtractor;
import com.omiads.device.DeviceClient;
import com.omiads.device.DeviceListView;
import com.omiads.device.DeviceListener;
import com.omiads.device.DeviceState;
import com.omiads.model.AdsData;
import com.omiads.model.AdsUtility;
import com.omiads.model.DevicePackages;
import com.omiads.view.CircleView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.regex.Pattern;

/**
 * Created by kanak on 21/12/16.
 */

public class WifiListActivity extends Activity implements
        FileTransferListener,
        ServerListener,
        ImageDisplayListener,
        FileRcdListener,
        DeviceListener,
        PackageListener,
        ReportListener
{
    private FrameLayout parentLayout                    = null;
    private CircleView circleView                       = null;
    private DeviceListView deviceListView               = null;
    private Server server                               = null;
    private TextView response                           = null;
    private EditText editTextAddress, editTextPort      = null;
    private Button buttonConnect, buttonClear           = null;
    private ImageDisplayListener imageDisplayListener   = null;
    private ImageView   adsImageView                    = null;
    private FileRcdListener receivedListener    = null;
    private WifiManager wifiManager             = null;
    private List<ScanResult> results            = null;
    private NetworkInfo     networkInfo;
    private String strMyAdminSSID = "MyAccessPoint";
    private String strSecondarySSID = "MyAccessPoint1";
    private boolean bIsAdminAvailable   = false;
    private boolean bIsSecondaryAdmin   = false;
    private boolean bIsWifiConnected;
    private boolean bIsHotspotEnable;
    private File[] nextWeekFiles;
    private ArrayList<AdsData> adsData                         = null;
    private DevicePackages devicePackages           = null;
    private PackageListener packageListener;
    private ReportListener reportListener;

    private static final int MY_PERMISSIONS_REQUEST_ALL = 1;
    private static final int SCAN_WIFI_REQUEST_ALL = 2;

    private DeviceListener clientConnectListener;
    private PowerManager.WakeLock wl    = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        parentLayout    = new FrameLayout(this);

        this.setContentView(parentLayout);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        clientConnectListener   = this;
        serverListener          = this;
        receivedListener        = this;
        imageDisplayListener    = this;
        packageListener         = this;
        reportListener          = this;

        wifiManager = (WifiManager) getSystemService(Context.WIFI_SERVICE);
        wifiManager.setWifiEnabled(true);

        PowerManager pm = (PowerManager) getSystemService(Context.POWER_SERVICE);
        wl = pm.newWakeLock(PowerManager.SCREEN_DIM_WAKE_LOCK, "My Tag");
        wl.acquire();

        server = new Server(WifiListActivity.this, serverListener, imageDisplayListener);

        circleView  = new CircleView(this);
        deviceListView  = new DeviceListView(this);
        adsImageView    = new ImageView(this);
        adsImageView.setLayoutParams(new ViewGroup.LayoutParams(FrameLayout.LayoutParams.MATCH_PARENT, FrameLayout.LayoutParams.MATCH_PARENT));
        adsImageView.setBackgroundColor(Color.WHITE);
        parentLayout.addView(circleView);
        parentLayout.addView(deviceListView);
        parentLayout.addView(adsImageView);

        IntentFilter scanIntent = new IntentFilter();
        scanIntent.addAction(WifiManager.SCAN_RESULTS_AVAILABLE_ACTION);
        scanIntent.addAction(WifiManager.WIFI_STATE_CHANGED_ACTION);
        this.registerReceiver(scanReceiver, scanIntent);

        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && checkSelfPermission(Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED){
            requestPermissions(new String[]
                    {
                            Manifest.permission.ACCESS_COARSE_LOCATION,
                            Manifest.permission.ACCESS_FINE_LOCATION,
                            Manifest.permission.WRITE_SETTINGS,
                            Manifest.permission.READ_PHONE_STATE
                    }, SCAN_WIFI_REQUEST_ALL);
        } else {

            wifiManager.startScan();
            scanHandler.removeCallbacks(scanRunnable);
            scanHandler.postDelayed(scanRunnable, 1000);

        }

        Calendar c = Calendar.getInstance();
        int seconds = c.get(Calendar.SECOND);

        System.out.println(" ========================= " + (seconds));
        timeUpdateHandler.removeCallbacks(timeUpdateRunnable);
        timeUpdateHandler.postDelayed(timeUpdateRunnable, (60000 - seconds));
    }

    PanelDetailsTask panelDetailsTask;
    Handler packageHandler  = new Handler();
    Runnable   packageRunnable  = new Runnable() {
        @Override
        public void run() {
            adsImageView.setImageResource(R.drawable.noads);
            adsImageView.setVisibility(View.VISIBLE);
            panelDetailsTask = new PanelDetailsTask(WifiListActivity.this, packageListener, receivedListener);
            panelDetailsTask.execute();
        }
    };

    @Override
    protected void onResume() {
        super.onResume();

        if (ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]
                    {
                            Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.ACCESS_WIFI_STATE,
                            Manifest.permission.CHANGE_NETWORK_STATE, Manifest.permission.CHANGE_WIFI_STATE,
                            Manifest.permission.ACCESS_NETWORK_STATE,
                            Manifest.permission.ACCESS_COARSE_LOCATION,
                            Manifest.permission.ACCESS_FINE_LOCATION,
                            Manifest.permission.READ_EXTERNAL_STORAGE
                    },  MY_PERMISSIONS_REQUEST_ALL);
        }
    }



    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_ALL: {
                if (grantResults.length > 0) {
                    boolean bIsAllPermission    = false;
                    for (int index = 0; index < grantResults.length; index++) {
                        if(grantResults[index] == PackageManager.PERMISSION_GRANTED) {
                            bIsAllPermission = true;
                        } else {
                            bIsAllPermission = false;
                            break;
                        }
                    }
                    if(bIsAllPermission) {
                        packageHandler.postDelayed(packageRunnable, 1000);
                    }
                }
                return;
            }
            case SCAN_WIFI_REQUEST_ALL: {
                wifiManager.startScan();
                scanHandler.removeCallbacks(scanRunnable);
                scanHandler.postDelayed(scanRunnable, 1000);
            }
        }
    }

    Handler reportHashTagHandler   = new Handler();
    Runnable reportHashTagRunnable   = new Runnable() {
        @Override
        public void run() {
            //new ReportCollectionTask(WifiListActivity.this, "10:00", "11:00", "RUNTIME", reportListener).execute();
            if(reportCollectionTask == null) {
                reportCollectionTask    = new ReportCollectionTask(WifiListActivity.this, device_uid, "10:00", "11:00", "RUNTIME", reportListener);
                reportCollectionTask.execute();
            } else {
                reportCollectionTask.cancel(true);
                reportCollectionTask    = null;
                reportCollectionTask    = new ReportCollectionTask(WifiListActivity.this, device_uid, "10:00", "11:00", "RUNTIME", reportListener);
                reportCollectionTask.execute();
            }
            reportHashTagHandler.postDelayed(reportHashTagRunnable, 10 * (60 * 1000));

        }
    };
    private ReportCollectionTask    reportCollectionTask    = null;


    Handler timeUpdateHandler   = new Handler();
    Runnable timeUpdateRunnable = new Runnable() {
        @Override
        public void run() {
            Calendar c = Calendar.getInstance();
            int hours = c.get(Calendar.HOUR);
            int minutes = c.get(Calendar.MINUTE);
            int day = c.get(Calendar.DAY_OF_WEEK);
            int week = c.get(Calendar.WEEK_OF_YEAR);
            int year    = c.get(Calendar.YEAR);
            if(day == 1) {
                day = 7;
            } else if(day == 2) {
                day = 1;
            } else if(day == 3) {
                day = 2;
            } else if(day == 4) {
                day = 3;
            } else if(day == 5) {
                day = 4;
            } else if(day == 6) {
                day = 5;
            } else if(day == 7) {
                day = 6;
            }
            System.out.println("  ******************************** ");
            System.out.println(" ============= Day " + c.get(Calendar.DATE));
            System.out.println(" ============= Min " + c.get(Calendar.MINUTE));
            System.out.println(" ============= Sec " + c.get(Calendar.SECOND));
            System.out.println(" ============= Hr " + c.get(Calendar.HOUR));
            if(adsData != null)
                System.out.println(" ============= Ads Size " + adsData.size());
            System.out.println("  ******************************** ");

            String currentminutes = "";
            String currenthours = "";
            String currentday = "" + day;
            if(minutes < 10) {
                currentminutes= "0" + minutes;
            } else {
                currentminutes = "" + minutes;
            }
            if(hours < 10) {
                currenthours = "0" + hours;
            } else {
                currenthours = "" + hours;
            }
            if (day == 7 &&currenthours.equals("06") && currentminutes.equals("30")) {
                packageHandler.postDelayed(packageRunnable, 1000);
            }
            if(adsData != null && adsData.size() > 0) {
                for (int posindex = 0; posindex < adsData.size();posindex++) {
                    AdsData ads = adsData.get(posindex);
                    //System.out.println(" ======= currentday " + day + " DATE : " + date + "/" + month + "/" + year);

                    if(ads != null && ads.getSlotIds().size() > 0) {
                        ArrayList<String> slots = ads.getSlotIds();

                        for (int index = 0; index < slots.size(); index++) {
                            String time = slots.get(index);
                            String starthour = time.charAt(0) + "" + time.charAt(1);
                            String startminutes = time.charAt(2) + "" + time.charAt(3);
                            String endhour = time.charAt(4) + "" + time.charAt(5);
                            String endtminutes = time.charAt(6) + "" + time.charAt(7);

                            System.out.println(" ============= Ads Size " +   ads.getSlotIds() + " : " + slots.get(index));

                            if (currentday.equals(ads.getPackageId()) && starthour.equals(currenthours) && startminutes.equals(currentminutes)) {
                                currentSlotMap = slots.get(index);
                                System.out.println( " =====  Current Date : " + currenthours+ ":" + currentminutes);
                                System.out.println(" ======= starthour " + index+ " : " + starthour + " : " + startminutes);
                                System.out.println(" ======= endhour " + index+ " : " + endhour + " : " + endtminutes);

                                imagesList    = ads.getSlotsList();
                                currentWeekNo    = "" + (week);
                                currentYearNo    = "" + year;
                                currentDayId    = "" + ads.getPackageId();
                                currentIndex    = 0;
                                adsLoopHandler.removeCallbacks(adsLoopRunnable);
                                adsLoopHandler.postDelayed(adsLoopRunnable, 0);
                                Toast.makeText(WifiListActivity.this, " Current Day :  " + currentday + " Week : " + week + " SLOT : " + time, Toast.LENGTH_LONG).show();
                                break;
                            }
                            if (currentday.equals(ads.getPackageId()) && endhour.equals(currenthours) && endtminutes.equals(currentminutes)) {
                                System.out.println(" ======= endhour  : " + endhour);
                                System.out.println(" ======= endtminutes  : " + endtminutes);
                                adsImageView.setVisibility(View.VISIBLE);
                                adsImageView.setImageResource(R.drawable.noads);
                                //adsLoopHandler.removeCallbacks(adsLoopRunnable);

                                //break;
                            }
                        }
                    }
                }
            }

            timeUpdateHandler.removeCallbacks(timeUpdateRunnable);
            timeUpdateHandler.postDelayed(timeUpdateRunnable, 1000 * 60);

        }
    };

    boolean bIsImageRunning;
    //private ArrayList<HashMap<String, String>> imagesList;
    HashMap<String,  ArrayList<HashMap<String, String>>> imagesList;
    private String currentWeekNo    = "";
    private String currentYearNo    = "";
    private String currentDayId    = "";
    private String currentSlotMap    = "";

    private int currentIndex    = 0;
    private Handler adsLoopHandler  = new Handler();
    private Runnable adsLoopRunnable    = new Runnable() {
        @Override
        public void run() {
            if(imagesList != null && imagesList.get(currentSlotMap).size() > currentIndex) {
                HashMap<String, String> images = imagesList.get(currentSlotMap).get(currentIndex);
                int runnableTime    = (60000 / (imagesList.get(currentSlotMap).size())) - 5000 ;
                System.out.println("======================== runnableTime : " + runnableTime + " ; " + imagesList.get(currentSlotMap).size());
                //String slide_id = images.get("slide_id");
                String img = images.get("img");
                String strPath = Environment.getExternalStorageDirectory()
                        + "/TricurveAdminPanel/"
                        + currentWeekNo + "_"
                        + currentYearNo
                        + "/"
                        + currentDayId
                        + "/slides/"
                        + img;
                System.out.println(" ======= currentWeekNo  : " + currentWeekNo);
                System.out.println(" ======= Current Running Ads  : " + strPath);

                File imgFilePath = new File(strPath);
                if (strPath.toLowerCase().endsWith(".png")
                        || strPath.toLowerCase().endsWith(".jpg")
                        || strPath.toLowerCase().endsWith(".jpeg")) {
                    Bitmap bitmap = BitmapFactory.decodeFile(strPath);
                    adsImageView.setImageBitmap(bitmap);
                }
                adsLoopHandler.removeCallbacks(adsLoopRunnable);
                adsLoopHandler.postDelayed(adsLoopRunnable, runnableTime);
            } else {
                imagesList = null;
                System.out.println(" ======= Failed...");
            }
            adsImageView.setVisibility(View.VISIBLE);

            currentIndex++;



        }
    };



    @Override
    protected void onPause() {
        super.onPause();

    }

    public static boolean setHotspotEnabled(String ssidName, Context context) {
        WifiManager	wifiManager	= (WifiManager) context.getSystemService(Context.WIFI_SERVICE);
        wifiManager.setWifiEnabled(false);
        WifiConfiguration netConfig = new WifiConfiguration();
        netConfig.SSID = ssidName;
        boolean apstatus = false;
        try {
            Method method = wifiManager.getClass().getMethod("setWifiApEnabled", WifiConfiguration.class, boolean.class);
            method.setAccessible(true);
            apstatus = (boolean)method.invoke(wifiManager, netConfig, true);
        } catch (Exception e) {
            Log.e(context.getClass().toString(),""+ e.getLocalizedMessage());
            e.printStackTrace();
        }
        return apstatus;
    }

    public static boolean setHotspotName(String ssidName, Context context) {
        try {
            WifiManager wifiManager = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);
            Method getConfigMethod = wifiManager.getClass().getMethod("getWifiApConfiguration");
            WifiConfiguration wifiConfig = (WifiConfiguration) getConfigMethod.invoke(wifiManager);
            wifiConfig.SSID = ssidName;
            wifiConfig.allowedKeyManagement.set(WifiConfiguration.KeyMgmt.NONE);
            Method setConfigMethod = wifiManager.getClass().getMethod("setWifiApConfiguration", WifiConfiguration.class);
            setConfigMethod.invoke(wifiManager, wifiConfig);
            return true;
        }
        catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    public static boolean setHotspotDisabled(String ssidName, Context context) {
        WifiManager	wifiManager	= (WifiManager) context.getSystemService(Context.WIFI_SERVICE);
        wifiManager.setWifiEnabled(false);
        WifiConfiguration netConfig = new WifiConfiguration();
        netConfig.SSID = ssidName;
        boolean apstatus = false;
        try {
            apstatus = (boolean) wifiManager.getClass().getMethod("setWifiApEnabled", WifiConfiguration.class, boolean.class)
                    .invoke(wifiManager, netConfig, false);
        } catch (Exception e) {
            Log.e(context.getClass().toString(),""+ e.getLocalizedMessage());
            e.printStackTrace();
        }
        return apstatus;
    }

    BroadcastReceiver scanReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {

            if(intent.getAction().equals(WifiManager.SCAN_RESULTS_AVAILABLE_ACTION)) {


            } else if (intent.getAction().equals(ConnectivityManager.CONNECTIVITY_ACTION)) {
                networkInfo = intent.getParcelableExtra(WifiManager.EXTRA_NETWORK_INFO);
                if(networkInfo != null && wifiManager != null && wifiManager.getConfiguredNetworks() != null && wifiManager.getConnectionInfo() != null) {
                    if (networkInfo != null && networkInfo.getDetailedState() != null && networkInfo.getDetailedState().equals(NetworkInfo.DetailedState.DISCONNECTED)) {

                    }
                    else if (networkInfo != null && networkInfo.getDetailedState() != null && networkInfo.getDetailedState().equals(NetworkInfo.DetailedState.CONNECTED))
                    {

                    }
                }
            } else if (intent.getAction().equals(WifiManager.SUPPLICANT_STATE_CHANGED_ACTION) && wifiManager != null
                    && wifiManager.getConfiguredNetworks() != null
                    && wifiManager.getConnectionInfo() != null) {
                if (wifiManager.getConnectionInfo().getSupplicantState().equals(SupplicantState.ASSOCIATING)) {

                } else if (wifiManager.getConnectionInfo().getSupplicantState().equals(SupplicantState.ASSOCIATED)) {

                } else if (wifiManager.getConnectionInfo().getSupplicantState().equals(SupplicantState.AUTHENTICATING)) {

                } else if (wifiManager.getConnectionInfo().getSupplicantState().equals(SupplicantState.DISCONNECTED)) {

                } else if (wifiManager.getConnectionInfo().getSupplicantState().equals(SupplicantState.INACTIVE)) {
                    if (wifiManager.getConfiguredNetworks().size() > 0) {

                    } else {

                    }
                } else if (wifiManager.getConnectionInfo().getSupplicantState().equals(SupplicantState.COMPLETED)) {


                }
            }
            else if (intent.getAction().equals(WifiManager.RSSI_CHANGED_ACTION)) {

            }
        }
    };


    private boolean checkSystemWritePermission() {
        boolean retVal = true;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            retVal = Settings.System.canWrite(this);
            Log.d("", "Can Write Settings: " + retVal);
            if(retVal){
                Toast.makeText(this, "Write allowed :-)", Toast.LENGTH_LONG).show();
            }else{
                Intent intent = new Intent(Settings.ACTION_MANAGE_WRITE_SETTINGS);
                intent.setData(Uri.parse("package:" + WifiListActivity.this.getPackageName()));
                WifiListActivity.this.startActivityForResult(intent, CODE_WRITE_SETTINGS_PERMISSION);
                Toast.makeText(this, "Write not allowed :-(", Toast.LENGTH_LONG).show();
            }
        }
        return retVal;
    }

    static int CODE_WRITE_SETTINGS_PERMISSION = 1001;

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == CODE_WRITE_SETTINGS_PERMISSION && Settings.System.canWrite(this)){
            //Log.d("TAG", " ===================== CODE_WRITE_SETTINGS_PERMISSION success");            //do your code
        }
    }




    List<HashMap<String, String>> connectedDeviceList;
    @Override
    public void sendDeviceList(List<HashMap<String, String>> connectedList) {
        connectedDeviceList = connectedList;

        this.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Log.v("SERVER", " ====> Display Device Called ");
                deviceListView.setVisibility(View.GONE);
                circleView.displayPeerDevices(connectedDeviceList, WifiListActivity.this);
            }
        });

    }

    private String defaultDirectoryLocation = Environment.getExternalStorageDirectory() + "/TricurveAdminPanel/";

    @Override
    public void showAds(String strPath, String target, String day_id, String password) {
        adsImageView.setVisibility(View.GONE);
        Log.d("SERVER", " ====> File Path to Disply : " + strPath);

        //ZipExtractor csUnZip = new ZipExtractor(WifiListActivity.this, strPath, defaultDirectoryLocation, receivedListener, "", "");
        //csUnZip.execute();

         adsData = new ArrayList<AdsData>();
        Toast.makeText(WifiListActivity.this, "File Downloaded successfully...", Toast.LENGTH_LONG).show();
        //Environment.getExternalStorageDirectory() + "/TricurveAdminPanel/fb.zip"
        ZipExtractor csUnZip = new ZipExtractor(WifiListActivity.this,
                strPath,
                target,
                receivedListener,
                password);
        csUnZip.execute();


        AdsData ads = AdsUtility.getInstance().getAdsRunnableList(
                target + "/data.json",
                day_id);
        adsData.add(ads);
    }

    private int     imagePosition   = 0;
    private boolean runAds;
    File[] files;

    @Override
    public void onFileReceived() {
        files = null;
        //imageHandler.removeCallbacks(imageRunnable);
        //imageHandler.postDelayed(imageRunnable, 0);
    }

    @Override
    public void onFileDownloading() {
        imagePosition   = 0;
        //imageHandler.removeCallbacks(imageRunnable);
    }

    @Override
    public void onDeviceConnected(DeviceState deviceState) {
        if(deviceState == DeviceState.CONNECTED) {
            triggerClient(strCurrentDeviceIp, true);
            deviceListView.deviceConnected();
        } else if(deviceState == DeviceState.SUCCESS){
            deviceListView.fileShareDone();
            Toast.makeText(getApplicationContext(), "File successfully shared to client!!!", Toast.LENGTH_SHORT).show();
        } else {
            deviceListView.deviceFailedToConnect();
            Toast.makeText(getApplicationContext(), "Device failed to connect, Please retry again!!!", Toast.LENGTH_SHORT).show();
        }

    }

    private String  device_uid;
    @Override
    public void onPackageDownloaded(ArrayList<AdsData> adsDatas, String deviceid) {
        System.out.println(" ================ Downloaded ");
        adsData    = adsDatas;
        device_uid  = deviceid;

        reportHashTagHandler.postDelayed(reportHashTagRunnable, 1000);

    }

    @Override
    public void onReportUpdated() {

    }


    public class WifiConfigManager extends AsyncTask<Void, Object, Object> {
        private final Pattern HEX_DIGITS = Pattern.compile("[0-9A-Fa-f]+");

        private final WifiManager wifiManager;
        String selectedSSID;
        String password;
        //String networkType;

        public WifiConfigManager(WifiManager wifiManager, String selectedSSID,
                                 String password) {
            this.wifiManager = wifiManager;
            if (wifiManager.isWifiEnabled() == false) {
                Toast.makeText(getApplicationContext(), "wifi is disabled..making it enabled", Toast.LENGTH_LONG).show();
                wifiManager.setWifiEnabled(true);
            }
            this.selectedSSID = selectedSSID;
            this.password = password;
        }

        @Override
        protected void onPostExecute(Object result) {
            super.onPostExecute(result);
            packageHandler.postDelayed(packageRunnable, 1000);
        }

        @Override
        protected String doInBackground(Void... args) {
            /*if (networkType.contains(WIFI_SECURITY_TYPE_OPEN)
                    || (!networkType.contains(WIFI_SECURITY_TYPE_WPA)
                    && !networkType.contains(WIFI_SECURITY_TYPE_WEP) && !networkType
                    .contains("802.1"))) {*/
                changeNetworkUnEncrypted(wifiManager, selectedSSID);
            /*} else {
                if (password == null || password.length() == 0) {
                }

                if (networkType.contains(WIFI_SECURITY_TYPE_WEP)) {
                    changeNetworkWEP(wifiManager, selectedSSID, password);
                } else if (networkType.contains(WIFI_SECURITY_TYPE_WPA)) {
                    changeNetworkWPA(wifiManager, selectedSSID, password);
                }
            }*/
            return "SUCCESS";
        }

        /**
         * Update the network: either create a new network or modify an existing
         * network
         *
         * @param config
         *            the new network configuration
         * @return network ID of the connected network.
         */
        private void updateNetwork(WifiManager wifiManager,
                                   WifiConfiguration config) {
            Integer foundNetworkID = findNetworkInExistingConfig(wifiManager,
                    config.SSID);
            if (foundNetworkID != null) {
                wifiManager.removeNetwork(foundNetworkID);
                wifiManager.saveConfiguration();
            }
            int networkId = wifiManager.addNetwork(config);
            if (networkId >= 0) {
                if (wifiManager.enableNetwork(networkId, true)) {
                    wifiManager.saveConfiguration();
                }
            }

            Log.d("OMIADS", "========== Current Network Id : " + networkId);

        }

        private WifiConfiguration changeNetworkCommon(String selectedSSID) {


            WifiConfiguration config = new WifiConfiguration();
            config.allowedAuthAlgorithms.clear();
            config.allowedGroupCiphers.clear();
            config.allowedKeyManagement.clear();
            config.allowedPairwiseCiphers.clear();
            config.allowedProtocols.clear();
            // Android API insists that an ascii SSID must be quoted to be
            // correctly handled.
            config.SSID = '\"' + selectedSSID + '\"';//quoteNonHex(selectedSSID);
            config.hiddenSSID = false;// wifiResult.isHidden();
            return config;
        }

        // Adding a WEP network
        private void changeNetworkWEP(WifiManager wifiManager,
                                      String selectedSSID, String password) {
            WifiConfiguration config = changeNetworkCommon(selectedSSID);
            config.wepKeys[0] = quoteNonHex(password, 10, 26, 58); // 10, 26,
            // and 58
            config.wepTxKeyIndex = 0;
            config.allowedKeyManagement.set(WifiConfiguration.KeyMgmt.NONE);

            config.allowedProtocols.set(WifiConfiguration.Protocol.WPA); // For
            // WPA
            config.allowedProtocols.set(WifiConfiguration.Protocol.RSN); // For
            // WPA2

            config.allowedAuthAlgorithms
                    .set(WifiConfiguration.AuthAlgorithm.OPEN);
            config.allowedAuthAlgorithms
                    .set(WifiConfiguration.AuthAlgorithm.SHARED);

            config.allowedPairwiseCiphers
                    .set(WifiConfiguration.PairwiseCipher.TKIP);
            config.allowedPairwiseCiphers
                    .set(WifiConfiguration.PairwiseCipher.CCMP);

            config.allowedGroupCiphers.set(WifiConfiguration.GroupCipher.CCMP);
            config.allowedGroupCiphers.set(WifiConfiguration.GroupCipher.TKIP);
            config.allowedGroupCiphers.set(WifiConfiguration.GroupCipher.WEP40);
            config.allowedGroupCiphers
                    .set(WifiConfiguration.GroupCipher.WEP104);

            updateNetwork(wifiManager, config);
        }

        // Adding a WPA or WPA2 network
        private void changeNetworkWPA(WifiManager wifiManager,
                                      String selectedSSID, String password) {
            WifiConfiguration config = changeNetworkCommon(selectedSSID);
            // Hex passwords that are 64 bits long are not to be quoted.
            config.preSharedKey = quoteNonHex(password, 64);

            config.allowedKeyManagement.set(WifiConfiguration.KeyMgmt.WPA_PSK);
            config.allowedKeyManagement.set(WifiConfiguration.KeyMgmt.WPA_EAP);
            config.allowedKeyManagement
                    .set(WifiConfiguration.KeyMgmt.IEEE8021X);

            config.allowedProtocols.set(WifiConfiguration.Protocol.WPA); // For
            // WPA
            config.allowedProtocols.set(WifiConfiguration.Protocol.RSN); // For
            // WPA2

            config.allowedAuthAlgorithms
                    .set(WifiConfiguration.AuthAlgorithm.OPEN);
            config.allowedAuthAlgorithms
                    .set(WifiConfiguration.AuthAlgorithm.SHARED);

            config.allowedPairwiseCiphers
                    .set(WifiConfiguration.PairwiseCipher.TKIP);
            config.allowedPairwiseCiphers
                    .set(WifiConfiguration.PairwiseCipher.CCMP);

            config.allowedGroupCiphers.set(WifiConfiguration.GroupCipher.TKIP);
            config.allowedGroupCiphers.set(WifiConfiguration.GroupCipher.CCMP);
            updateNetwork(wifiManager, config);
        }

        // Adding an open, unsecured network
        private void changeNetworkUnEncrypted(WifiManager wifiManager, String selectedSSID) {
            WifiConfiguration config = changeNetworkCommon(selectedSSID);
            config.allowedKeyManagement.set(WifiConfiguration.KeyMgmt.NONE);
            updateNetwork(wifiManager, config);
        }

        private Integer findNetworkInExistingConfig(WifiManager wifiManager,
                                                    String selectedSSID) {
            List<WifiConfiguration> existingConfigs = wifiManager
                    .getConfiguredNetworks();
            if(existingConfigs!=null) {
                for (WifiConfiguration existingConfig : existingConfigs) {
                    if (existingConfig.SSID != null && existingConfig.SSID.equals(selectedSSID)) {
                        return existingConfig.networkId;
                    }
                }
            }
            return null;
        }

        private String quoteNonHex(String value, int... allowedLengths) {
            return isHexOfLength(value, allowedLengths) ? value
                    : convertToQuotedString(value);
        }

        /**
         * Encloses the incoming string inside double quotes, if it isn't
         * already quoted.
         *
         * @param string
         *            the input string
         * @return a quoted string, of the form "input". If the input string is
         *         null, it returns null as well.
         */
        private String convertToQuotedString(String string) {
            if (string == null || string.length() == 0) {
                return null;
            }
            // If already quoted, return as-is
            if (string.charAt(0) == '"'
                    && string.charAt(string.length() - 1) == '"') {
                return string;
            }

            return '\"' + string + '\"';
        }

        /**
         * @param value
         *            input to check
         * @param allowedLengths
         *            allowed lengths, if any
         * @return true if value is a non-null, non-empty string of hex digits,
         *         and if allowed lengths are given, has an allowed length
         */
        private boolean isHexOfLength(CharSequence value, int... allowedLengths) {
            if (value == null || !HEX_DIGITS.matcher(value).matches()) {
                return false;
            }
            if (allowedLengths.length == 0) {
                return true;
            }

            for (int length : allowedLengths) {
                if (value.length() == length) {
                    return true;
                }
            }
            return false;
        }

    }

    private ServerListener  serverListener;


    View.OnClickListener connectToWifiListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            connectToWifi(WifiListActivity.this, "MyAccessPoint");
            server = new Server(WifiListActivity.this, serverListener, imageDisplayListener);
           Log.d("SERVER", " ======>   " + server.getIpAddress() + ":" + server.getPort());
        }
    };

    public int getClientList() {
        int macCount = 0;
        BufferedReader br = null;
        connectedDeviceList    = new ArrayList<HashMap<String, String>>();
        try {
            br = new BufferedReader(new FileReader("/proc/net/arp"));
            String line;
            while ((line = br.readLine()) != null) {
                String[] splitted = line.split(" +");
                if (splitted != null ) {
                    String mac = splitted[3];
                    System.out.println("Mac : Outside If "+ mac );
                    if (mac.matches("..:..:..:..:..:..")) {
                        macCount++;

                        HashMap<String, String> mapDevice   = new HashMap<String, String>();
                        mapDevice.put("macaddress", mac);
                        mapDevice.put("ipaddress", splitted[0]);
                        connectedDeviceList.add(mapDevice);
                        System.out.println("Mac : "+ mac + " IP Address : "+splitted[0] );
                        System.out.println("Mac_Count  " + macCount + " MAC_ADDRESS  "+ mac);
                        Toast.makeText(getApplicationContext(),
                                "Mac_Count  " + macCount + "   MAC_ADDRESS  "
                                        + mac, Toast.LENGTH_SHORT).show();

                    }
                }
            }
        } catch(Exception e) {
            e.printStackTrace();
        }
        //circleView.setVisibility(View.GONE);
        deviceListView.setVisibility(View.VISIBLE);
        deviceListView.displayPeerDevices(connectedDeviceList, WifiListActivity.this);
        return macCount;
    }

    public static void connectToWifi(Context context, String networkSSID) {

        WifiConfiguration conf = new WifiConfiguration();
        conf.allowedKeyManagement.set(WifiConfiguration.KeyMgmt.NONE);

        WifiManager wifiManager = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);
        wifiManager.addNetwork(conf);


        List<WifiConfiguration> list = wifiManager.getConfiguredNetworks();
        for (WifiConfiguration i : list) {
            Log.d("" + " |connectToWifi()", "i.SSID=> " + i.SSID);

            if (i.SSID != null && i.SSID.equals("\"" + networkSSID + "\"")) {
                wifiManager.disconnect();
                wifiManager.enableNetwork(i.networkId, true);
                wifiManager.reconnect();

                Log.d("" + " |connectToWifi()", "IF > i.SSID=> " + i.SSID);

                break;
            }else {Log.e("" + " |connectToWifi()", "ELSE - FAILED TO CONNECT > i.SSID=> " + i.SSID);}
        }
    }

    @Override
    protected void onDestroy() {
        if(server != null) {
            server.onDestroy();
        }
        if(wl != null) {
            wl.release();
        }
        try {
            if (scanReceiver != null)
                this.unregisterReceiver(scanReceiver);
        }catch (Exception e) {
            e.printStackTrace();
        }
        WifiManager manage = (WifiManager) getSystemService(WIFI_SERVICE);
        //manage.setWifiEnabled(false);
        if(setHotspotDisabled("MyAccessPoint", this)) {
            Toast.makeText(this, "Disconnecting...", Toast.LENGTH_SHORT).show();
        }

        super.onDestroy();
    }


    @Override
    public void sharedDone() {
        Toast.makeText(getApplicationContext(), "Action Will Be Taken Care", Toast.LENGTH_SHORT).show();

    }

    private void connectData() {
        results = wifiManager.getScanResults();
        if(results != null && results.size() > 0) {
            System.out.println( " ======================= " + results.size() );
            for (int index = 0; index < results.size(); index++) {
                if (strMyAdminSSID.equals("" + results.get(index).SSID)) {
                    bIsAdminAvailable = true;
                    break;
                }
            }
            for (int index = 0; index < results.size(); index++) {
                if (strSecondarySSID.equals("" + results.get(index).SSID)) {
                    bIsSecondaryAdmin = true;
                    break;
                }
            }
            if (bIsAdminAvailable) {
                if(!bIsWifiConnected) {
                    adsImageView.setVisibility(View.GONE);
                    deviceListView.setVisibility(View.GONE);
                    circleView.setVisibility(View.VISIBLE);
                    Toast.makeText(WifiListActivity.this, "Connecting to Admin!!!", Toast.LENGTH_LONG).show();
                    new WifiConfigManager(wifiManager, strMyAdminSSID, "").execute();
                    bIsWifiConnected = true;
                }
            } else if(bIsSecondaryAdmin) {
                if(!bIsWifiConnected) {
                    bIsWifiConnected = true;
                    runAds = true;
                    adsImageView.setVisibility(View.GONE);
                    deviceListView.setVisibility(View.GONE);
                    circleView.setVisibility(View.VISIBLE);
                    Toast.makeText(WifiListActivity.this, "Connecting to Admin!!!", Toast.LENGTH_LONG).show();
                    new WifiConfigManager(wifiManager, strSecondarySSID, "").execute();
                    packageHandler.postDelayed(packageRunnable, 10 * 1000);
                }
            } else {

                File directory = new File(defaultDirectoryLocation);
                nextWeekFiles = directory.listFiles();

                packageHandler.postDelayed(packageRunnable, 1000);
                if (nextWeekFiles != null && nextWeekFiles.length > 0) {
                    if(checkSystemWritePermission() && !bIsHotspotEnable) {
                        bIsHotspotEnable = true;
                        if (setHotspotEnabled(strSecondarySSID, WifiListActivity.this)) {
                            runAds = true;
                            //imageHandler.removeCallbacks(imageRunnable);
                            adsImageView.setVisibility(View.GONE);
                            setHotspotName("MyAccessPoint1", WifiListActivity.this);

                        } else {
                            Toast.makeText(WifiListActivity.this, "Hotspot failed to open!!!", Toast.LENGTH_LONG).show();
                        }
                        deviceListHandler.postDelayed(deviceListRunnable, 10000);
                    }
                } else {
                    Toast.makeText(WifiListActivity.this, "Ads Not Available!!!", Toast.LENGTH_LONG).show();
                }

            }
        } else {

            if(!bIsHotspotEnable) {
                adsImageView.setVisibility(View.GONE);
                if(getClientList() > 0) {
                    bIsHotspotEnable    = true;
                    packageHandler.postDelayed(packageRunnable, 1000);
                    Toast.makeText(WifiListActivity.this, "Hotspot Not Enabled!!!", Toast.LENGTH_LONG).show();
                } else {
                    scanHandler.removeCallbacks(scanRunnable);
                    scanHandler.postDelayed(scanRunnable, 10000);
                    Toast.makeText(WifiListActivity.this, "Hotspot Enabled Scanning!!!", Toast.LENGTH_LONG).show();
                }

            } else {
                Toast.makeText(WifiListActivity.this, "Hotsot Enabled!!!", Toast.LENGTH_LONG).show();
                runAds = true;
                adsImageView.setVisibility(View.GONE);
                deviceListHandler.postDelayed(deviceListRunnable, 10000);
            }

        }
    }

    Handler deviceListHandler = new Handler();
    Runnable deviceListRunnable = new Runnable() {
        @Override
        public void run() {
            getClientList();
            deviceListHandler.removeCallbacks(deviceListRunnable);
            deviceListHandler.postDelayed(deviceListRunnable, 10000);
        }
    };

    Handler scanHandler = new Handler();
    Runnable scanRunnable = new Runnable() {
        @Override
        public void run() {
            connectData();

        }
    };

    private String  strCurrentDeviceIp  = "";
    @Override
    public void triggerClient(String ipAddress, boolean isFiles) {
        strCurrentDeviceIp  = ipAddress;
        //Toast.makeText(getApplicationContext(), "Connecting to Panel App", Toast.LENGTH_SHORT).show();
        if(isFiles == true){

            String mainPath = Environment.getExternalStorageDirectory().getPath() + "/TricurveAdminPanel/fb.zip";
            File mainFile = new File(mainPath);
            if(mainFile.exists()) {
                Log.d("CLIENT", " =====> File Exists Sending Data === " + mainPath);
                DeviceClient myClient = new DeviceClient(ipAddress, 8080, mainFile, clientConnectListener);
                myClient.execute();
            }
        }
        else
        {
            JSONArray jsonArray = new JSONArray();

            for (int index =0; index < connectedDeviceList.size(); index++) {
                JSONObject deviceData = new JSONObject();
                try {
                    deviceData.put("ipaddress", connectedDeviceList.get(index).get("ipaddress"));
                    deviceData.put("macaddress", connectedDeviceList.get(index).get("macaddress"));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                jsonArray.put(deviceData);
            }
            JSONObject  data    = new JSONObject();
            try {
                data.put("data", jsonArray);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            DeviceClient csDeviceClient = new DeviceClient(ipAddress, 8080, data.toString(), clientConnectListener);
            csDeviceClient.execute();
        }
    }


}
