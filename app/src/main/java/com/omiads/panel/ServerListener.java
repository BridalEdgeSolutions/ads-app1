package com.omiads.panel;

import java.util.HashMap;
import java.util.List;

/**
 * Created by kanak on 22/12/16.
 */

public interface ServerListener {

    void sendDeviceList(List<HashMap<String, String>> connectedDeviceList);

}
