package com.omiads.panel;

/**
 * Created by Kanagaraj on 03-01-2017.
 */
public interface FileRcdListener {
    void onFileReceived();
    void onFileDownloading();
}
